﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cursoYoutube.Models
{
    public class CVPA_Producto
    {
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public double Precio { get; set; }
        public bool IsDisponible { get; set; }

    }

    public class CVPA_Cliente
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string MostrarNombre { get; set; }
        public Int32 Edad { get; set; }
        public string Profesion  { get; set; }
    }
}