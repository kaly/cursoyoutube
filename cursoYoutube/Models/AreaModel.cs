﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cursoYoutube.Models
{
    public class AreaModel
    {
        StoreProcedureDataContext contexto = new StoreProcedureDataContext();

        public List<C_Area> ListaAreas() {

            List<C_Area> lista = new List<C_Area>();
            var consulta = contexto.PR_Listar_Area();

            foreach (var area in consulta) {
                C_Area a = new C_Area();

                a.id_area = area.id_area;
                a.descripcion = area.descripcion;

                lista.Add(a);
            }
            return lista;
        }

        public C_Area BuscarArea(int id) {
            C_Area a = new C_Area();
            try
            {
                var consulta = contexto.PR_Buscar_Area(id);

                foreach( var area in consulta){
                    a.id_area = area.id_area;
                    a.descripcion = area.descripcion;
                }
            }
            catch (Exception)  { throw; }

            return a;
        }

        public string InsertarArea(C_Area a) {
            string resultado = string.Empty;
            try
            {
                contexto.PR_Insertar_Area(a.descripcion);
                resultado = "OK";
            }
            catch (Exception ex) { resultado=ex.Message; }
            return resultado;
        }

        public string ActualizarArea(C_Area a) {
            string resultado = string.Empty;
            try
            {
                contexto.PR_Actualizar_Area(a.id_area, a.descripcion);
                resultado = "OK";
            }
            catch (Exception ex)
            {
                resultado = ex.Message;                
            }
            return resultado;
        }

        public string EliminarArea(int id) {
            string resultado = string.Empty;
            try
            {
                contexto.PR_Eliminar_Area(id);
                resultado = "OK";
            }
            catch (Exception ex)
            {
                resultado = ex.Message;                
            }
            return resultado;
        
        }
    }
}

// SCRIPT CREACION DE LAS TABLAS Y DE LOS PROCEDIMIENTOS ALMACENADOS
/* 
 create table C_Area(
	id_area int not null primary key,
	descripcion nvarchar(50) not null
);

create table C_Empleado(
	id_empleado int not null primary key,
	nombre nvarchar(50) not null,
	id_area int not null references C_Area
);

insert into C_Area (id_area, descripcion)values (1,'gerencia');
insert into C_Area (id_area, descripcion)values (2,'Técnico');
insert into C_Area (id_area, descripcion)values (3,'Almacén');

insert into C_Empleado (id_empleado, nombre, id_area) values (1,'Luis',1);
insert into C_Empleado (id_empleado, nombre, id_area) values (2,'Jose',2);
insert into C_Empleado (id_empleado, nombre, id_area) values (3,'Monse',2);
insert into C_Empleado (id_empleado, nombre, id_area) values (4,'Tomas',2);
insert into C_Empleado (id_empleado, nombre, id_area) values (5,'Lucia',3);

select * from C_Area;
select * from C_Empleado;

create procedure PR_Listar_Area 
as
select * from C_Area
go

create procedure PR_Buscar_Area
@id int
as
select * from C_Area where id_area = @id
go

create procedure PR_Insertar_Area
@descripcion nvarchar(50)
as
-- validamos si no existen dos areas con el mismo nombre
if not exists(select * from C_Area where descripcion = @descripcion)
begin
declare @id int
select id=isnull(max(id_area),0) +1 from C_Area

insert into C_Area(id_area,descripcion) values (@id, @descripcion)

end
go

create procedure PR_Actualizar_Area
@id int,
@descripcion nvarchar(50)

as

--validamos que no existen dos areas con el mismo name
if not exists(select * from C_Area where descripcion=@descripcion)

begin
update C_Area set descripcion=@descripcion where id_area=@id
end 
go

create procedure PR_Eliminar_Area
@id int

as
delete C_Area where id_area=@id
go



 
 
 */