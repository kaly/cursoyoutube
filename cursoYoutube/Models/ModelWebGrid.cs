﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;

namespace cursoYoutube.Models
{
    // PLa forma simple de obtener todos los datos de la tabla
    public class ModelCliente1: IDisposable
    {
        private readonly bdCursoYoutubeEntities _datos = new bdCursoYoutubeEntities();

        public IEnumerable<C_Cliente> ObtenerClientes()
        {
            return _datos.C_Clientes.ToList();
        }

        public void Dispose()
        {
            _datos.Dispose();
        }
    }

    // forma mas completa de obtener los datos de la tabla. Esta forma mejora el rendimiento
    public class ModelCliente2 : IDisposable
    {
        private readonly bdCursoYoutubeEntities _datos = new bdCursoYoutubeEntities();

        public IEnumerable<C_Cliente> ObtenerClientes()
        {
            return _datos.C_Clientes.ToList();
        }

        public int ContarClientes()
        {
            return _datos.C_Clientes.Count();
        }

        // el criterio de ordenación se pasa como un string = "it.Nombre ASC"
        public IEnumerable<C_Cliente> ObtenerPaginaDeClientes(
                int paginaActual,
                int ClientesPorPagina,
                string criterioOrdenacion)
        {
            if (paginaActual < 1) paginaActual = 1;


            return _datos.C_Clientes
                .OrderBy(criterioOrdenacion)
                .Skip((paginaActual - 1) * ClientesPorPagina)
                .Take(ClientesPorPagina).ToList();
        }
 
        public void Dispose()
        {
            _datos.Dispose();            
        }

    }

    // Obtener datos de oforma obtimizada y con filtros
    public class ModelClienteConFiltro : IDisposable
    {
        private readonly bdCursoYoutubeEntities _datos = new bdCursoYoutubeEntities();

        public IEnumerable<C_Cliente> ObtenerClientes()
        {
            return _datos.C_Clientes.ToList();
        }

        public int ContarClientes(string buscar = null, int? minId = null, int? maxId = null )
        {
            IQueryable<C_Cliente> query = _datos.C_Clientes;
            query = queryClientesFiltrados(buscar, minId, maxId, query);
            return query.Count();
        }

        private static IQueryable<C_Cliente> queryClientesFiltrados(string buscar, int? minId, int? maxId, IQueryable<C_Cliente> query)
        {
            if (!string.IsNullOrWhiteSpace(buscar))
                query = query.Where(p => p.nombre.Contains(buscar) || p.apellidos.Contains(buscar));
            if (minId != null)
                query = query.Where(p => p.id_cliente >= minId);
            if (maxId != null)
                query = query.Where(p => p.id_cliente <= maxId);

            return query;
        }

        // el criterio de ordenación se pasa como un string = "it.Nombre ASC"
        public IEnumerable<C_Cliente> ObtenerPaginaDeClientes(int paginaActual, int ClientesPorPagina, 
                        string criterioOrdenacion, string buscar, int? minId, int? maxId)
        {
            if (paginaActual < 1) paginaActual = 1;

            var query = (IQueryable<C_Cliente>)_datos.C_Clientes.OrderBy(criterioOrdenacion);

            query = queryClientesFiltrados(buscar, minId, maxId, query);

            return query
                    .Skip((paginaActual - 1) * ClientesPorPagina)
                    .Take(ClientesPorPagina)
                    .ToList();
        }

        public void Dispose()
        {
            _datos.Dispose();
        }

    }
    
}