﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cursoYoutube.Models
{
    public class VentasMes
    {
        public int id { get; set; }
        public string mes { get; set; }
        public int valor { get; set; }
        public ICollection<DetalleMes> DetalleMes { get; set; }
    }

    public class DetalleMes
    { 
        public int id { get; set; }
        public string producto { get; set; }
        public int valor { get; set; }
        public VentasMes VentasMes { get; set; }
    }
}