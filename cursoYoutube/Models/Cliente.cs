﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cursoYoutube.Models
{
    public class Cliente
    {
        public int codigo { get; set;}
        public string nombre { get; set;}
        public string apellidos { get; set; }
        public int cod_direccion { get; set; }
        public string direccion { get; set; }
        public string localidad { get; set; }
        public string provincia { get; set; }
        public string cp { get; set; }

    }
}