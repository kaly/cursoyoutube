﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cursoYoutube.Models
{
    public class ddlCascada
    {
        // sustituto de una tabla relacionada
        private static Dictionary<string, string[]> datos = new Dictionary<string, string[]>
        {
            {"animal", new[] {"Perro", "Gato", "Pez", "Pato"}},
            {"Vegetal", new[] {"Flor", "Árbol", "Espárrago"}},
            {"Mineral", new[] {"Cuarzo", "Pirita", "Feldespato", "Cromo", "Cobre"}}
        };

        public static IEnumerable<string> GetCategorias()
        {
            return datos.Keys;
        }

        public static IEnumerable<string> GetElementosPorCategoria(string categoria)
        {
            string[] els = null;
            datos.TryGetValue(categoria, out els);
            return els;
        }

    }
}