﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class ClienteController : Controller
    {
        //
        // GET: /Cliente/
        bdCursoYoutubeEntities entidad = new bdCursoYoutubeEntities();


        public ActionResult Index()
        {
            var listaClientes = entidad.C_Clientes;
            return View(listaClientes.ToList());
        }

        public ActionResult ListaDirecciones()
        {
            var listaClDirecciones = entidad.C_Direcciones;
            return View(listaClDirecciones.ToList());
        }

        public ActionResult ListaClientePorCP(string cp)
        {
            var modelo = from c in entidad.C_Clientes 
                         join d in entidad.C_Direcciones on c.id_cliente equals d.id_cliente
                         where d.cp == cp
                         select c;
            return View(modelo.ToList());
        }

        public ActionResult ListaClienteDireccion()
        {
            var modelo = from p in entidad.C_Clientes
                         join c in entidad.C_Direcciones on p.id_cliente equals c.id_cliente

                         select new Cliente
                         {
                             codigo = p.id_cliente,
                             nombre = p.nombre,
                             apellidos = p.apellidos,
                             cod_direccion = c.id_direccion,
                             direccion = c.calle,
                             localidad = c.localidad,
                             provincia = c.provincia,
                             cp = c.cp
                         };
            return View(modelo.ToList());

        }


    }
}
