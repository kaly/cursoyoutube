﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class CVPA2Controller : Controller
    {
        //
        // GET: /CVPA2/
        
        private readonly List<VentasMes> ventasMes = new List<VentasMes>()
        {
            new VentasMes()
            { 
                id = 1, 
                mes = "Enero",                 
                valor=200000, 
                DetalleMes = new List<DetalleMes>(){
                   new DetalleMes(){ id = 1, producto = "Producto 1", valor = 50000},
                   new DetalleMes(){ id = 1, producto = "Producto 2", valor = 50000},
                   new DetalleMes(){ id = 1, producto = "Producto 3", valor = 50000},
                   new DetalleMes(){ id = 1, producto = "Producto 4", valor = 50000}
                }
            },
            new VentasMes()
            { 
                id = 2, 
                mes = "Febrero", 
                valor=200000, 
                DetalleMes = new List<DetalleMes>(){
                   new DetalleMes(){ id = 1, producto = "Producto 1", valor = 100000},
                   new DetalleMes(){ id = 1, producto = "Producto 2", valor = 100000}
                }
            },
        };

        public ActionResult Index()
        {
            return View(ventasMes);
        }

        public ActionResult ViewDetails(int id)
        {
            var detalle = ventasMes
                            .Where(c => c.id == id)
                            .Select(c => c.DetalleMes)
                            .FirstOrDefault();
            return PartialView("_Details", detalle);
        }
    }
}
