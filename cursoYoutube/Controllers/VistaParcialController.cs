﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cursoYoutube.Controllers
{
    public class VistaParcialController : Controller
    {
        //
        // GET: /VistaParcial/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Suma(int numero1, int numero2)
        {
            ViewBag.Suma = numero1 + numero2;
            return PartialView();            
        }

    }
}
