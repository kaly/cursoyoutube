﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;
using cursoYoutube.ViewModels;

namespace cursoYoutube.Controllers
{
    public class WebGridFiltrosController : Controller
    {
        //
        // GET: /WebGridFiltros/

        public ActionResult Index(int page =1, string sort = "nombre", string sortDir = "ASC",
                                  string buscar = null, int? minId = null, int? maxId = null) 
        {
            const int ClientesPorPagina = 5;
            var clientes = new ModelClienteConFiltro().ObtenerPaginaDeClientes(page, ClientesPorPagina, "it." + sort + " " + sortDir,
                                                                               buscar, minId, maxId);
            var numClientes = new ModelClienteConFiltro().ContarClientes(buscar, minId, maxId);
            var datos = new PaginaDeClientesConFiltroViewModel()
                {
                    NumeroDeClientes = numClientes,
                    ClientesPorPagina = ClientesPorPagina,
                    Clientes = clientes
                };



            return View(datos);
        }

    }
}
