﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;
using cursoYoutube.ViewModels;

namespace cursoYoutube.Controllers
{
    public class WebGridController : Controller
    {
        //
        // GET: /WebGrid/

        public ActionResult Index()
        {
            var datos = new ModelCliente1().ObtenerClientes(); 
            return View(datos);
        }

        public ActionResult Index2( int page =1, string sort = "nombre", string sortDir = "ASC")
        {
            const int ClientesPorPagina = 5;
            var clientes = new ModelCliente2().ObtenerPaginaDeClientes(page, ClientesPorPagina, "it." + sort + " " + sortDir); 
            var numClientes = new ModelCliente2().ContarClientes();
            var datos = new PaginaDeClientesViewModel()
                {
                    NumeroDeClientes = numClientes,
                    ClientesPorPagina = ClientesPorPagina,
                    Clientes = clientes
                };



            return View(datos);
        }

        public ActionResult WebGridAjax()
        {
            var datos = new ModelCliente1().ObtenerClientes();
            return View(datos);
        }
    }
}
