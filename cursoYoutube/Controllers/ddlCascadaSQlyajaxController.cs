﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class ddlCascadaSQlyajaxController : Controller
    {
        //
        // GET: /ddlCascadaSQlyajax/
        bdCursoYoutubeEntities db = new bdCursoYoutubeEntities();

        public ActionResult Index()
        {
            ViewBag.Clientes = db.C_Clientes.ToList();
            ViewBag.Direcciones = db.C_Direcciones.ToList();
            return View();
        }


        // -----------------------------------------------------------

        private IList<C_Direccion> GetDirecciones(int idCliente)
        {
            return db.C_Direcciones.Where(d => d.id_cliente == idCliente).ToList();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadDireccionesPorCliente(string idCliente)
        {
            var DireccionesList = this.GetDirecciones(Convert.ToInt32(idCliente));
            var DireccionesData = DireccionesList.Select(d => new SelectListItem()
            {
                Text = d.calle,
                Value = d.id_direccion.ToString(),
            });
            return Json(DireccionesData, JsonRequestBehavior.AllowGet);
        }

        
        // -----------------------------------------------------------
        //public IList<C_Cliente> ObtenerClientes()
        //{           
        //    var query = from c in db.C_Clientes
        //                select c;
        //    var content = query.ToList<C_Cliente>();
        //    return content;
        //}
        //public IList<C_Direccion> ObtenerDireccionesPorCliente(int idCliente)
        //{
        //    var query = from d in db.C_Direcciones
        //                where d.id_cliente == idCliente
        //                select d;
        //    var content = query.ToList<C_Direccion>();
        //    return content;
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
