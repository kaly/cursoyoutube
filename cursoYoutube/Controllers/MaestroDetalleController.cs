﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class MaestroDetalleController : Controller
    {
        NorthwindEntities contexto = null;


        //
        // GET: /MaestroDetalle/

        public ActionResult Index()
        {
            List<Orders> ordenes = new List<Orders>();
            using(contexto = new NorthwindEntities())
            {
                ordenes = contexto.Orders.Take(5).ToList();
            }

            return View(ordenes);
        }

        public JsonResult GetOrderDeatailsByOrderId(int id)
        {
            try
            {
                using (contexto = new NorthwindEntities())
                {
                    IQueryable<Order_Details>order_details = contexto.Order_Details.Where(od => od.OrderID == id);
                    List<OrderDetail> orderDetails = new List<OrderDetail>();
                    foreach (var od in order_details) 
                    {
                        OrderDetail orderDetail = new OrderDetail();
                        orderDetail.OrderID = od.OrderID;
                        orderDetail.ProductName = contexto.Products.Where(p => p.ProductID == od.ProductID).SingleOrDefault().ProductName;
                        orderDetail.UnitPrice = od.UnitPrice;
                        orderDetail.Quantity = od.Quantity;
                        orderDetail.Discount = od.Discount;
                        orderDetail.OrderID = od.OrderID;
                        orderDetails.Add(orderDetail);
                    }

                    return Json(new { results = orderDetails }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public class OrderDetail 
        
        {
            public int OrderID { get; set; }
            public string ProductName { get; set; }
            public decimal UnitPrice { get; set; }
            public short Quantity { get; set; }
            public float Discount { get; set; }
        }

    }
}
