﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class CVPAController : Controller
    {
        List<CVPA_Cliente> objCliente = new List<CVPA_Cliente>();
        List<CVPA_Producto> objProducto = new List<CVPA_Producto>();

        //
        // GET: /CVPA/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormContext form)
        {
            return View();
        }

        public ActionResult ObtenerCliente()
        {
            CVPA_Cliente c;
            c = new CVPA_Cliente();
            c.Nombre="Pepito";
            c.Apellido = "Gonzalez";
            c.MostrarNombre = c.Nombre + " " + c.Apellido;
            c.Edad = 30;
            c.Profesion = "Camarero";
            objCliente.Add(c);

            c = new CVPA_Cliente();
            c.Nombre = "Tomas";
            c.Apellido = "Filipe";
            c.MostrarNombre = c.Nombre + " " + c.Apellido;
            c.Edad = 21;
            c.Profesion = "Cartero";
            objCliente.Add(c);

            return View("DetalleCliente", objCliente);
        }

        public ActionResult ObtenerProducto()
        {
            CVPA_Producto p;
            p = new CVPA_Producto();
            p.Nombre = "Tornillo";
            p.Fecha = DateTime.Now;
            p.Precio = 55.99;
            p.IsDisponible = true;
            objProducto.Add(p);

            p = new CVPA_Producto();
            p.Nombre = "Llave inglesa";
            p.Fecha = DateTime.Now;
            p.Precio = 54.99;
            p.IsDisponible = true;
            objProducto.Add(p);

            p = new CVPA_Producto();
            p.Nombre = "Sandalias";
            p.Fecha = DateTime.Now;
            p.Precio = 520.45;
            p.IsDisponible = false;
            objProducto.Add(p);

            return View("DetalleProducto", objProducto);
        }

        public ActionResult Vacio()
        {
            return View("vacio");
        }

    }
}
