﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;


namespace cursoYoutube.Controllers
{
    public class ddlCascadaController : Controller
    {
        //
        // GET: /ddlCascada/

        public ActionResult Index()
        {
            /* FORMA ANTIGUA MVC1 y MVC2*/
            // ViewData["ddCategoria"] = new SelectList(ddlCascada.GetCategorias());
            // ViewData["ddElemento"] = new SelectList(new [] {"(Selecciona)"});

            /* FORMA MODERNA MVC3 y +*/
            ViewBag.ddCategoria = new SelectList(ddlCascada.GetCategorias());
            ViewBag.ddElemento = new SelectList(new[] { "(Selecciona)" });

            //Para saber mas sobre ViewData, ViewBag y TempData --> http://sebys.com.ar/2013/10/22/asp-net-mvc-viewdata-viewbag-y-tempdata/

            return View();
        }

        public ActionResult GetElementos(string categoria)
        {
            IEnumerable<string> elementos = ddlCascada.GetElementosPorCategoria(categoria);
            if(elementos==null)
                throw new ArgumentException("Categoria " + categoria + " no es correcta" );
            
          System.Threading.Thread.Sleep(1000); // simulamos tiempo de proceso ...

          return Json(elementos, JsonRequestBehavior.AllowGet);   // JsonRequestBehavior.AllowGet --> Es importante para que no se produzca un error de permisos
        }

    }
}
