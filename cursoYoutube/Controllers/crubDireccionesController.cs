﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class crubDireccionesController : Controller
    {
        private bdCursoYoutubeEntities db = new bdCursoYoutubeEntities();

        //
        // GET: /crubDirecciones/

        public ActionResult Index()
        {
            var c_direcciones = db.C_Direcciones.Include("C_Cliente");
            return View(c_direcciones.ToList());
        }

        //
        // GET: /crubDirecciones/Details/5

        public ActionResult Details(int id = 0)
        {
            C_Direccion c_direccion = db.C_Direcciones.Single(c => c.id_direccion == id);
            if (c_direccion == null)
            {
                return HttpNotFound();
            }
            return View(c_direccion);
        }

        //
        // GET: /crubDirecciones/Create

        public ActionResult Create()
        {
            ViewBag.id_cliente = new SelectList(db.C_Clientes, "id_cliente", "nombre");
            return View();
        }

        //
        // POST: /crubDirecciones/Create

        [HttpPost]
        public ActionResult Create(C_Direccion c_direccion)
        {
            if (ModelState.IsValid)
            {              
                db.C_Direcciones.AddObject(c_direccion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cliente = new SelectList(db.C_Clientes, "id_cliente", "nombre", c_direccion.id_cliente);
            return View(c_direccion);
        }

        //
        // GET: /crubDirecciones/Edit/5

        public ActionResult Edit(int id = 0)
        {
            C_Direccion c_direccion = db.C_Direcciones.Single(c => c.id_direccion == id);
            if (c_direccion == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cliente = new SelectList(db.C_Clientes, "id_cliente", "nombre", c_direccion.id_cliente);
            return View(c_direccion);
        }

        //
        // POST: /crubDirecciones/Edit/5

        [HttpPost]
        public ActionResult Edit(C_Direccion c_direccion)
        {
            if (ModelState.IsValid)
            {
                db.C_Direcciones.Attach(c_direccion);
                db.ObjectStateManager.ChangeObjectState(c_direccion, System.Data.EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_cliente = new SelectList(db.C_Clientes, "id_cliente", "nombre", c_direccion.id_cliente);
            return View(c_direccion);
        }

        //
        // GET: /crubDirecciones/Delete/5

        public ActionResult Delete(int id = 0)
        {
            C_Direccion c_direccion = db.C_Direcciones.Single(c => c.id_direccion == id);
            if (c_direccion == null)
            {
                return HttpNotFound();
            }
            return View(c_direccion);
        }

        //
        // POST: /crubDirecciones/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            C_Direccion c_direccion = db.C_Direcciones.Single(c => c.id_direccion == id);
            db.C_Direcciones.DeleteObject(c_direccion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}