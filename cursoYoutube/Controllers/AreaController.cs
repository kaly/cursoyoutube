﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cursoYoutube.Models;

namespace cursoYoutube.Controllers
{
    public class AreaController : Controller
    {
        //
        // GET: /Area/

        public AreaModel modelo = new AreaModel();

        public ActionResult Index()
        {
            return View(modelo.ListaAreas());
        }

        public ActionResult Detalles(int id) {
            var area = modelo.BuscarArea(id);
            return View(area);
        }

        public ActionResult Crear() {
            return View();
        }

        [HttpPost]
        public ActionResult Crear(C_Area a) {

            try
            {
                if (modelo.InsertarArea(a).Equals("OK"))
                {
                    return RedirectToAction("Index");
                }
                else {
                    return RedirectToAction("Crear");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult Editar(int id) {
            var area = modelo.BuscarArea(id);
            return View(area);
        }

        [HttpPost]
        public ActionResult Editar(int id, C_Area a) {

            try
            {
                C_Area area = new C_Area();
                area.id_area = id;
                area.descripcion = a.descripcion;

                if (modelo.ActualizarArea(area).Equals("OK"))
                {
                    return RedirectToAction("Index");
                }
                else {
                    return RedirectToAction("Editar");                
                }
            }
            catch (Exception)
            {
                return View();
            }
        }

        public ActionResult Eliminar(int id) {


            var area = modelo.BuscarArea(id);
            return View(area);
        }

        [HttpPost]
        public ActionResult Eliminar(int id, FormCollection coleccion) {
            try
            {
                if (modelo.EliminarArea(id).Equals("OK"))
                {
                    return RedirectToAction("Index");
                }
                else
                { 
                    return RedirectToAction("Eliminar");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }



    }
}
