﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using cursoYoutube.Models;

namespace cursoYoutube.ViewModels
{
    public class PaginaDeClientesViewModel
    {
        public int NumeroDeClientes { get; set; }
        public int ClientesPorPagina { get; set; }
        public IEnumerable<C_Cliente> Clientes { get; set; }
    }

    public class PaginaDeClientesConFiltroViewModel
    {
        public int NumeroDeClientes { get; set; }
        public int ClientesPorPagina { get; set; }
        public IEnumerable<C_Cliente> Clientes { get; set; }
    }
}