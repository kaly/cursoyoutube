﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using cursoYoutube.Models;

namespace cursoYoutube.ViewModels
{
    public class MultiplesModelosViewModel
    {
        public List<Course> allCourses { get; set; }
        public List<Student> allStudents { get; set; }
        public List<Faculty> allFaculties { get; set; }
    }
}